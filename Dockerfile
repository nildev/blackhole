FROM centurylink/ca-certs

COPY ./blackhole /blackholed
COPY ./io.blackhole.conf /blackhole.conf

CMD ["./blackholed", "--config=./blackhole.conf"]

EXPOSE 8080