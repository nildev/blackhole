package config

// Config type
type Config struct {
	Verbosity   int
	EndpointURL string
	IP          string
	Port        string
}
