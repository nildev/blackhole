package version

import (
	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/github.com/coreos/go-semver/semver"
)

const Version = "0.1.0"

var SemVersion semver.Version

func init() {
	sv, err := semver.NewVersion(Version)
	if err != nil {
		panic("bad version string!")
	}
	SemVersion = *sv
}
