package server

import (
	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/bitbucket.org/nildev/lib/log"
	"bitbucket.org/nildev/blackhole/config"
	"bitbucket.org/nildev/blackhole/endpoints"
	"bitbucket.org/nildev/blackhole/version"
	"net/http"
)

// Server type
type Server struct {
	stop    chan bool
	cfg     config.Config
	handler http.Handler
}

// New type
func New(cfg config.Config) (*Server, error) {
	srv := Server{
		cfg:     cfg,
		stop:    nil,
		handler: endpoints.Router(),
	}
	return &srv, nil
}

// Run server
func (s *Server) Run() {
	log.Infof("Starting blackholed service [%s:%s]@%s", s.cfg.IP, s.cfg.Port, version.Version)
	s.stop = make(chan bool)

	go func() {
		log.Infof("Starting HTTP server ...")
		if err := http.ListenAndServe(s.cfg.IP+":"+s.cfg.Port, s.handler); err != nil {
			log.WithField("error", err).Fatal("Unable to create listener")
		}
	}()
}

// Stop server
func (s *Server) Stop() {
	close(s.stop)
}

// Purge server
func (s *Server) Purge() {
}
