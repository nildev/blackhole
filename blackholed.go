package main // import "bitbucket.org/nildev/blackhole"

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/github.com/rakyll/globalconf"

	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/bitbucket.org/nildev/lib/log"
	"bitbucket.org/nildev/blackhole/config"
	"bitbucket.org/nildev/blackhole/server"
	"bitbucket.org/nildev/blackhole/version"

	// Import these as after code is generated it will be required
	_ "bitbucket.org/nildev/blackhole/Godeps/_workspace/src/bitbucket.org/nildev/lib/codegen"
	_ "bitbucket.org/nildev/blackhole/Godeps/_workspace/src/bitbucket.org/nildev/lib/utils"
)

const (
	DefaultConfigFile = "/etc/blackholed/blackholed.conf"
)

func main() {
	userset := flag.NewFlagSet("blackholed", flag.ExitOnError)
	printVersion := userset.Bool("version", false, "Print the version and exit")
	cfgPath := userset.String("config", "", fmt.Sprintf("Path to config file. blackholed will look for a config at %s by default.", DefaultConfigFile))

	err := userset.Parse(os.Args[1:])
	if err == flag.ErrHelp {
		userset.Usage()
		os.Exit(1)
	}

	args := userset.Args()
	if len(args) == 1 && args[0] == "version" {
		*printVersion = true
	} else if len(args) != 0 {
		userset.Usage()
		os.Exit(1)
	}

	if *printVersion {
		fmt.Println("blackholed version", version.Version)
		os.Exit(0)
	}

	log.Infof("Starting blackholed version %v", version.Version)

	cfgset := flag.NewFlagSet("selisd", flag.ExitOnError)
	cfgset.Int("verbosity", 0, "Logging level")
	cfgset.String("endpointurl", "", "Endpoint url")
	cfgset.String("ip", "", "Server IP to bind")
	cfgset.String("port", "", "Port to listen on")

	globalconf.Register("", cfgset)
	cfg, err := getConfig(cfgset, *cfgPath)
	if err != nil {
		log.Fatalf(err.Error())
	}

	log.Debugf("Creating Server")
	srv, err := server.New(*cfg)
	if err != nil {
		log.Fatalf("Failed creating Server: %v", err.Error())
	}
	srv.Run()

	reconfigure := func() {
		log.Infof("Reloading configuration from %s", *cfgPath)

		cfg, err := getConfig(cfgset, *cfgPath)
		if err != nil {
			log.Fatalf(err.Error())
		}

		log.Infof("Restarting server components")
		srv.Stop()

		srv, err = server.New(*cfg)
		if err != nil {
			log.Fatalf(err.Error())
		}
		srv.Run()
	}

	shutdown := func() {
		log.Infof("Gracefully shutting down")
		srv.Stop()
		srv.Purge()
		os.Exit(0)
	}

	writeState := func() {
		log.Infof("Dumping server state")

		encoded, err := json.Marshal(srv)
		if err != nil {
			log.Errorf("Failed to dump server state: %v", err)
			return
		}

		if _, err := os.Stdout.Write(encoded); err != nil {
			log.Errorf("Failed to dump server state: %v", err)
			return
		}

		os.Stdout.Write([]byte("\n"))

		log.Debugf("Finished dumping server state")
	}

	signals := map[os.Signal]func(){
		syscall.SIGHUP:  reconfigure,
		syscall.SIGTERM: shutdown,
		syscall.SIGINT:  shutdown,
		syscall.SIGUSR1: writeState,
	}

	listenForSignals(signals)
}

func getConfig(flagset *flag.FlagSet, userCfgFile string) (*config.Config, error) {
	opts := globalconf.Options{EnvPrefix: "BLACKHOLED_"}

	if userCfgFile != "" {
		// Fail hard if a user-provided config is not usable
		fi, err := os.Stat(userCfgFile)
		if err != nil {
			log.Fatalf("Unable to use config file %s: %v", userCfgFile, err)
		}
		if fi.IsDir() {
			log.Fatalf("Provided config %s is a directory, not a file", userCfgFile)
		}

		log.Infof("Using provided config file %s", userCfgFile)
		opts.Filename = userCfgFile

	} else if _, err := os.Stat(DefaultConfigFile); err == nil {
		log.Infof("Using default config file %s", DefaultConfigFile)
		opts.Filename = DefaultConfigFile
	} else {
		log.Infof("No provided or default config file found - proceeding without")
	}

	gconf, err := globalconf.NewWithOptions(&opts)
	if err != nil {
		return nil, err
	}

	gconf.ParseSet("", flagset)

	cfg := config.Config{
		Verbosity:   (*flagset.Lookup("verbosity")).Value.(flag.Getter).Get().(int),
		EndpointURL: (*flagset.Lookup("endpointurl")).Value.(flag.Getter).Get().(string),
		IP:          (*flagset.Lookup("ip")).Value.(flag.Getter).Get().(string),
		Port:        (*flagset.Lookup("port")).Value.(flag.Getter).Get().(string),
	}

	//if cfg.Verbosity > 0 {}

	return &cfg, nil
}

func listenForSignals(sigmap map[os.Signal]func()) {
	sigchan := make(chan os.Signal, 1)

	for k := range sigmap {
		signal.Notify(sigchan, k)
	}

	for true {
		sig := <-sigchan
		handler, ok := sigmap[sig]
		if ok {
			handler()
		}
	}
}
