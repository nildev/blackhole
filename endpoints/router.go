package endpoints

import (
	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/bitbucket.org/nildev/lib/router"
	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/github.com/codegangsta/negroni"
	"bitbucket.org/nildev/blackhole/Godeps/_workspace/src/github.com/gorilla/mux"
	"bitbucket.org/nildev/blackhole/gen"
	"net/http"
)

func Router() http.Handler {
	r := mux.NewRouter()

	RegisterRoutes(r)

	return negroni.New(
		negroni.Wrap(r),
	)
}

func RegisterRoutes(r *mux.Router) {
	routes := gen.BuildRoutes()

	for _, rt := range routes {
		r.PathPrefix(rt.BasePattern).Handler(negroni.New(
			negroni.Wrap(router.NewRouter(rt)),
		))
	}
}
